# Stupid python path shit.
# Instead just add darknet.py to somewhere in your python path
# OK actually that might not be a great idea, idk, work in progress
# Use at your own risk. or don't, i don't care

import cv2
import dlib
import numpy
import glob
import os
import sys
from skimage import io
from datetime import datetime
from matplotlib import pyplot as plt

#from PIL import Image, ImageDraw, ImageFont

#import sys, os
#import dlib

sys.path.append(os.path.join(os.getcwd(),'python/'))
PREDICTOR_PATH = "/home/mcw/Documents/ben/DMS_demo/darknet/dms/sp.dat"
predictor = dlib.shape_predictor(PREDICTOR_PATH)

import darknet as dn

net = dn.load_net("../../darknet/cfg/tiny-yolo-voc.cfg", "../../darknet/backup/tiny-yolo-voc_final.weights", 0)
meta = dn.load_meta("../../darknet/cfg/coco.data")
#r = dn.detect(net, meta, "MCW_Blink_Yawn_demo.mp4_00000000.jpg")


detector = dlib.get_frontal_face_detector()
#font1 = cv2.FONT_HERSHEY_COMPLEX_SMALL

def get_landmarks(im):
    dect = detector(im, 1)
    #print ("detector length",len(dect))
    if len(dect) == 0:
        return None
    return numpy.matrix([[p.x, p.y] for p in predictor(im, dect[0]).parts()])

def annotate_landmarks(im, ax, bbox, landmarks):
    height , width , layers = im.shape
    im = im.copy()
    eye_mouth_points = []
    index = 1
    #print ("landmarks size",landmarks.size)
    if landmarks == None:
        return im
    for idx, point in enumerate(landmarks):
        pos = (point[0, 0], point[0, 1])
        # Eye and Mouth Landmarks
        if idx in {36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68}:
           eye_mouth_points.append(point)
#           cv2.putText(im, str(index), pos,
#                   fontFace=cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
#                   fontScale=0.1,
#                   color=(0, 0, 255))
#           cv2.circle(im, pos, 1, color=(0, 255, 255))

           ax.text(pos[0], pos[1],
           '{:d}'.format(index),
           fontsize=3, color='blue')
           ax.add_patch(plt.Circle(pos,radius=0.5, color='g'))
           index += 1


    ear_right = (cv2.norm(eye_mouth_points[1], eye_mouth_points[5]) + cv2.norm(eye_mouth_points[2], eye_mouth_points[4])) / (2* cv2.norm (eye_mouth_points[0], eye_mouth_points[3]))
    #print 'EAR ratio right:: ', ear_right
    ear_left = (cv2.norm(eye_mouth_points[7], eye_mouth_points[11]) + cv2.norm(eye_mouth_points[8], eye_mouth_points[10])) / (2* cv2.norm (eye_mouth_points[6], eye_mouth_points[9]))
    #print 'EAR ratio left:: ', ear_left

    if ear_right <= 0.21 and ear_left <= 0.21:
        ax.text(bbox[2], bbox[3] - 60,
        'Blink',
        bbox=dict(facecolor='mediumseagreen', alpha=0.5),
        fontsize=12, color='white')
#        cv2.putText(im,'Blink',(((width) - (width/13)) - 22,(width/20) + 10),font1,1,(0,0,0),2)

    # Define Mouth Aspect ratio 
    mar = (cv2.norm(eye_mouth_points[25], eye_mouth_points[31]) + cv2.norm(eye_mouth_points[27], eye_mouth_points[29])) / (2* cv2.norm (eye_mouth_points[24], eye_mouth_points[28]))
#    print ("Mar Val is ", mar)
    if mar > 0.32:
        ax.text(bbox[2], bbox[3] - 12,
        'Yawn',
        bbox=dict(facecolor='mediumseagreen', alpha=0.5),
        fontsize=12, color='white')
#        cv2.putText(im,'Yawn',(((width) - (width/13)) - 18,(width/20) + 36),font1,1,(0,0,0),2)
#    return im

def vls_detection(im_list):
    for f in im_list:
        print("Processing file: {}".format(f))
        # Added by Shobi
        img = io.imread(f)

#        img = img[:, :, (2, 1, 0)]

        fig, ax = plt.subplots(figsize=(12, 12))
        ax.imshow(img, aspect='equal')

        filename = f.split('/')[-1]
#        print (filename)
        tstart = datetime.now()
        dets = dn.detect(net, meta, f)
        if dets:
            from operator import itemgetter
            index, element = max(enumerate(dets), key=itemgetter(1))
    #        print (index, element)

            thresh = 0.55
    #        index = 0
            x, y, w, h = dets[index][2]
            score = dets[index][1]
            category = dets[index][0]
#            print (category, score)
            if score > thresh :
                left  = int(x- w/2.)
                top = int(y- h/2.)
                right = int(left + w)
                bottom = int(top + h)
                bbox = [left,top,right,bottom]

                ax.add_patch(
                    plt.Rectangle((bbox[0], bbox[1]),
                                  bbox[2] - bbox[0],
                                  bbox[3] - bbox[1], fill=False,
                                  edgecolor='cadetblue', linewidth=1.5)
                    )

                ax.text(bbox[0], bbox[1] - 12,
                        '{:s}'.format(category),
                        bbox=dict(facecolor='mediumseagreen', alpha=0.5),
                        fontsize=12, color='white')
                if category == 'Frontal':
                     annotate_landmarks(img, ax, bbox ,get_landmarks(img))
    #                ax.imshow(image, aspect='equal')
        tend = datetime.now()
        tt = str((tend  - tstart).seconds)+'.'+str((tend - tstart).microseconds)
        print 'time taken :',tt
#                io.imsave(faces_folder_path+"out/"+filename,image)
        plt.axis('off')
        plt.savefig(faces_folder_path+"out/" + filename, bbox_inches='tight', pad_inches=0)
        plt.close()



faces_folder_path = sys.argv[1]

im_list = glob.glob(faces_folder_path+"/*.png")
im_list.sort()


vls_detection(im_list)

