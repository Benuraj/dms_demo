# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains demo script in order to detect pose (Pitch, Frontal, Yaw) alongside Blink and Yawn.

### How do I get set up? ###

* In order to set up build Darknet as well as Dlib as instructed in respective README
* Once both the tools are build from Darknet root folder run dms_demo.py script that resides inside examples folder.
* usage: python examples/dms_demo.py "path of jpg/png folder" 
* The results will be stored inside subfolder out inside input folder.


### Who do I talk to? ###

* Benuraj